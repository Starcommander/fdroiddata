Categories:Time,Writing
License:GPLv3
Web Site:
Source Code:https://github.com/douzifly/clear-todolist
Issue Tracker:https://github.com/douzifly/clear-todolist/issues

Auto Name:Clear List
Summary:Add todo items and alarms
Description:
Helps people to quickly capture what's on their mind and get a reminder later at
the right time. Included are two clear and beautiful material designed themes to
make use experience better. No remote server is used, data is kept on your
device.
.

Repo Type:git
Repo:https://github.com/douzifly/clear-todolist

Build:1.4.2,6
    commit=b23aa41dc02e8bc36d347e9ca39383483cd86cb2
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/android {/alintOptions {\nabortOnError false\n}\n' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4.2
Current Version Code:6
